import os
from flask import Flask, jsonify

app = Flask(__name__)

@app.route('/hostname', methods=['GET'])
def get_hostname():
    return os.uname().nodename

@app.route('/author', methods=['GET'])
def get_author():
    author_name = os.environ.get('AUTHOR', 'Unknown Author')
    return author_name

@app.route('/id', methods=['GET'])
def get_id():
    uuid = os.environ.get('UUID', 'Unknown UUID')
    return uuid

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
